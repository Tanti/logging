﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace FileSystem
{
    internal enum DirectoryContent
    {
        SubFolder,
        File
    }

    public class FileSystemVisitor
    {
        public EventHandler<FolderFoundArgs> FolderFoundHandler;
        public EventHandler<FileFoundArgs> FileFoundHandler;
        public EventHandler<FilteredFolderFoundArgs> FilteredFolderFoundHandler;
        public EventHandler<FilteredFileFoundArgs> FilteredFileFoundHandler;
        public EventHandler<EventArgs> StartHandler;
        public EventHandler<EventArgs> EndHandler;

        public bool IsStoppedWalking { get; set; }

        private readonly Func<FileSystemInfo, bool> _filterFolderAction;
        private readonly Func<FileSystemInfo, bool> _filterFileAction;
        private Logger logger;

        public FileSystemVisitor()
            : this(null, null)
        {
        }

        public FileSystemVisitor(Func<FileSystemInfo, bool> filterFolderAction, Func<FileSystemInfo, bool> filterFileAction)
        {
            _filterFolderAction = filterFolderAction;
            _filterFileAction = filterFileAction;
            logger = LogManager.GetLogger("info");
        }


        public IEnumerable<string> WalkDirectoryTree(string rootCatalog, int depth)
        {
            logger.Info("Starting WalkDirectoryTree method");
            if (!Directory.Exists(rootCatalog))
            {
                throw new ArgumentException(String.Format("{0} does not exist", rootCatalog));
            }
            StringBuilder directoryInfoSB = new StringBuilder();
            Stack<string> dirs = new Stack<string>(depth);

            dirs.Push(rootCatalog);

            var startHandler = StartHandler;
            if (startHandler != null)
            {
                startHandler(this, new EventArgs());
            }

            while (dirs.Count > 0 && !IsStoppedWalking)
            {
                string currentDirectory = dirs.Pop();
                string[] subDirs;
                string[] files;

                directoryInfoSB.AppendLine(String.Format("Folder {0} ", currentDirectory));

                try
                {
                    subDirs = Directory.GetDirectories(currentDirectory);
                    this.WriteDirectoryInfo(subDirs, directoryInfoSB, _filterFolderAction);
                    if (IsStoppedWalking)
                    {
                        continue;
                    }
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

                try
                {
                    files = Directory.GetFiles(currentDirectory);
                    this.WriteFileInfo(files, directoryInfoSB, _filterFileAction);
                    if (IsStoppedWalking)
                    {
                        continue;
                    }
                }
                catch (UnauthorizedAccessException e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }

                foreach (string subDir in subDirs)
                    dirs.Push(subDir);

                yield return directoryInfoSB.ToString();
                directoryInfoSB.Clear();
            }

            var endHandler = EndHandler;
            if (endHandler != null)
            {
                endHandler(this, new EventArgs());
            }

            logger.Info("Successful ending WalkDirectoryTree method");
        }


        //много if операторов из-за проверки объектов событий на null!
        //WriteDirectoryInfo и WriteFileInfo очень похожи, но я все-таки разделил этот функционал на 2 метода, т.к. они и так не очень читаемы из-за большого кол-ва if-ов.
        private void WriteDirectoryInfo(string[] folders, StringBuilder sb, Func<FileSystemInfo, bool> filterAction)
        {
            sb.Append("\t\tsub directories:    ");

            foreach (var folder in folders)
            {
                var directoryInfo = new DirectoryInfo(folder);

                if (filterAction != null)
                {
                    if (filterAction(directoryInfo))
                    {
                        if (FilteredFolderFoundHandler != null)
                        {
                            var filteredFolderFoundArgs = new FilteredFolderFoundArgs {DirectoryInfo = directoryInfo};
                            FilteredFolderFoundHandler(this, filteredFolderFoundArgs);
                            if (!filteredFolderFoundArgs.Exlude)
                            {
                                sb.Append(String.Format("{0} | ", directoryInfo.Name));
                            }
                        }
                    }
                }
                else
                {
                    sb.Append(String.Format("{0} | ", directoryInfo.Name));
                    if (FolderFoundHandler != null)
                    {
                        FolderFoundHandler(this, new FolderFoundArgs { DirectoryInfo = directoryInfo });
                    }
                }
            }
            sb.Remove(sb.Length - 3, 2);
            sb.AppendLine();
        }

        private void WriteFileInfo(string[] files, StringBuilder sb, Func<FileSystemInfo, bool> filterAction)
        {
            sb.Append("\t\tfiles:    ");

            foreach (var file in files)
            {
                var fileInfo = new FileInfo(file);

                if (filterAction != null)
                {
                    if (filterAction(fileInfo))
                    {
                        if (FilteredFileFoundHandler != null)
                        {
                            var filteredFileFoundArgs = new FilteredFileFoundArgs {FileInfo = fileInfo};
                            FilteredFileFoundHandler(this, filteredFileFoundArgs);
                            if (!filteredFileFoundArgs.Exlude)
                            {
                                sb.Append(String.Format("{0} | ", fileInfo.Name));
                            }
                        }
                    }
                }
                else
                {
                    sb.Append(String.Format("{0} | ", fileInfo.Name));
                    if (FileFoundHandler != null)
                    {
                        FileFoundHandler(this, new FileFoundArgs { FileInfo = fileInfo });
                    }
                }
            }
            sb.Remove(sb.Length - 3, 2);
            sb.AppendLine();
        }
    }

    public class FolderFoundArgs : EventArgs
    {
        public DirectoryInfo DirectoryInfo { get; set; }
    }

    public class FileFoundArgs : EventArgs
    {
        public FileInfo FileInfo { get; set; }
    }

    public class FilteredFolderFoundArgs : EventArgs
    {
        public DirectoryInfo DirectoryInfo { get; set; }
        public bool Exlude { get; set; }
    }

    public class FilteredFileFoundArgs : EventArgs
    {
        public FileInfo FileInfo { get; set; }
        public bool Exlude { get; set; }
    }
}
