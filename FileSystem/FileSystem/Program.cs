﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NLog;

namespace FileSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = LogManager.GetLogger("errors");
            Console.WindowWidth = 200;
            FileSystemVisitor fileSystemVisitor = new FileSystemVisitor(folderInfo => !folderInfo.Name.Contains("System"), fileInfo => fileInfo.Extension != ".secret");

            fileSystemVisitor.StartHandler += (sender, eventArgs) => Console.WriteLine("Walking has started");
            fileSystemVisitor.EndHandler += (sender, eventArgs) => Console.WriteLine("Walking has ended");
            fileSystemVisitor.FilteredFolderFoundHandler += (sender, foundArgs) =>
            {
                if (foundArgs.DirectoryInfo.Name == "msf-0 empty folder")
                {
                    ((FileSystemVisitor)sender).IsStoppedWalking = true;
                    Console.WriteLine("'msf-0 empty' folder was found => walking is stopped");
                }
            };
            fileSystemVisitor.FilteredFileFoundHandler += (sender, foundArgs) =>
            {
                if (foundArgs.FileInfo.Extension == ".secret")
                {
                    foundArgs.Exlude = true;
                }
            };

            try
            {
                foreach (var directory in fileSystemVisitor.WalkDirectoryTree(String.Format(@"{0}\Test catalog", Directory.GetCurrentDirectory()), 20))
                {
                    Console.WriteLine("-------------->info");
                    Console.WriteLine(directory);
                    Console.WriteLine("\n\n");
                }
            }
            catch (ArgumentException ex)
            {
                logger.Error(ex);
                throw;
            }

            Console.ReadLine();
        }
    }
}
