﻿using System;
using System.IO;
using FileSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileSystemTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Works_WithoutFilters_WithoutEventHanlders()
        {
            var fileSystemVisitor = new FileSystemVisitor();

            var expected =
                "Folder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog " +
                "\r\n\t\tsub directories:    msf-0 | msf-1 | msf-2 | msf-3 " +
                "\r\n\t\tfiles:    mf-1.txt | Новый текстовый документ.pdf " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-3 " +
                "\r\n\t\tsub directories:  " +
                "\r\n\t\tfiles:    msf-3 file-1.txt | msf-3 file-2.txt " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-2 " +
                "\r\n\t\tsub directories:    msf-2 subFolder1 " +
                "\r\n\t\tfiles:    msf-2 file-1.txt " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-2\\msf-2 subFolder1 " +
                "\r\n\t\tsub directories:  " +
                "\r\n\t\tfiles:    msf-2 subFolder1 file-1.txt " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-1" +
                " \r\n\t\tsub directories:  " +
                "\r\n\t\tfiles:    msf-1 file-1.txt | Новый текстовый документ.secret " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-0 " +
                "\r\n\t\tsub directories:    msf-0 empty folder " +
                "\r\n\t\tfiles:  " +
                "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-0\\msf-0 empty folder \r\n\t\tsub directories:  " +
                "\r\n\t\tfiles:  \r\n\n";

            string actual = "";

            foreach (var directory in fileSystemVisitor.WalkDirectoryTree(String.Format(@"{0}\Test catalog", Directory.GetCurrentDirectory()), 20))
            {
                Console.WriteLine(directory);
                actual += directory;
                actual += "\n";
            }

            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void Works_WithFilters_WithEventHanlders()
        {
            //фильтруем папку по содержанию в имени "System" и файл по расширению .secret
            FileSystemVisitor fileSystemVisitor = new FileSystemVisitor(folderInfo => !folderInfo.Name.Contains("System"), fileInfo => fileInfo.Extension != ".secret");

            //этими обработчиками события мы сообщаем о начале/конце обхода
            fileSystemVisitor.StartHandler += (sender, eventArgs) => Console.WriteLine("Walking has started");
            fileSystemVisitor.EndHandler += (sender, eventArgs) => Console.WriteLine("Walking has ended");

            //указываем, что, найдя msf-0 empty folder, мы должны остановить обход
            fileSystemVisitor.FilteredFolderFoundHandler += (sender, foundArgs) =>
            {
                if (foundArgs.DirectoryInfo.Name == "msf-0 empty folder")
                {
                    ((FileSystemVisitor)sender).IsStoppedWalking = true;
                    Console.WriteLine("'msf-0 empty' folder was found => walking is stopped");
                }
            };

            //если находим по фильтру файл с расширением .secret - не включаем его в выборку.
            fileSystemVisitor.FilteredFileFoundHandler += (sender, foundArgs) =>
            {
                if (foundArgs.FileInfo.Extension == ".secret")
                {
                    foundArgs.Exlude = true;
                }
            };

            var expected = "Folder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog " +
                           "\r\n\t\tsub directories:    msf-0 | msf-1 | msf-2 | msf-3 " +
                           "\r\n\t\tfiles:    mf-1.txt | Новый текстовый документ.pdf " +
                           "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-3 " +
                           "\r\n\t\tsub directories:  " +
                           "\r\n\t\tfiles:    msf-3 file-1.txt | msf-3 file-2.txt " +
                           "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-2 " +
                           "\r\n\t\tsub directories:    msf-2 subFolder1 " +
                           "\r\n\t\tfiles:    msf-2 file-1.txt " +
                           "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-2\\msf-2 subFolder1 " +
                           "\r\n\t\tsub directories:  " +
                           "\r\n\t\tfiles:    msf-2 subFolder1 file-1.txt " +
                           "\r\n\nFolder D:\\Mentoring Epam\\FileSystem\\FileSystemTests\\bin\\Debug\\Test catalog\\msf-1 " +
                           "\r\n\t\tsub directories:  " +
                           "\r\n\t\tfiles:    msf-1 file-1.txt \r\n\n";

            string actual = "";

            foreach (var directory in fileSystemVisitor.WalkDirectoryTree(String.Format(@"{0}\Test catalog", Directory.GetCurrentDirectory()), 20))
            {
                Console.WriteLine(directory);
                actual += directory;
                actual += "\n";
            }

            Assert.AreEqual(actual, expected);
        }
    }
}
